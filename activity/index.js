/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
            function registerNewUser (newUser) {
                
            let usernameAlreadyExist = registeredUsers.includes(newUser);

                if(usernameAlreadyExist) {
                    alert("Registration failed. Username already exists!");
                console.log("Register: " + newUser)
                } else {
                    registeredUsers.push(newUser);
                        alert("Thank you for registering!");
                console.log("Register: " + newUser);
            }
            };

            registerNewUser("Russel Viray");
            console.log("Registered Users: ")
            console.log(registeredUsers);

            registerNewUser("Gunther Smith");
            console.log("Registered Users: ")
            console.log(registeredUsers);

            console.log("")

    


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

            function AddFriend (newFriend) {
                let AddToList = registeredUsers.includes(newFriend);

                if(AddToList) {
                    friendsList.push(newFriend)
                    alert("You have added " + newFriend + " as a friend!");
                    console.log("Added a friend: ");
                    console.log(newFriend);
                } else {
                    alert("User not found!")
                    console.log("Added a friend: ");
                    console.log(newFriend);
                }
            }

            AddFriend("Russel Viray");
            console.log("Friends List:  ")
            console.log(friendsList);

            console.log("")

            AddFriend("Macie West");
            console.log("Friends List:  ")
            console.log(friendsList);

            console.log("")

            AddFriend("Chris Hemsworth");
            console.log("Friends List:  ")
            console.log(friendsList);

            console.log("")

            AddFriend("Peter Griffin");
            console.log("Friends List:  ")
            console.log(friendsList);

            console.log("")



/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/


            console.log("Display Friends: ");

            friendsList.forEach(function(entry) {
                console.log(entry);
            })
        
            console.log("")

            

    


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

            function numberOfRegFriends (regFriends) {
                let registeredfriends = friendsList.length;

                if(registeredfriends ++) {
                    alert("You currently have " + friendsList.length + " friends")
                } else {
                    alert("You currently have " + friendsList.length + " friends. Add one first.")
                }
            }

            numberOfRegFriends(friendsList.length)
                console.log("Display number of friends: " + friendsList.length);



/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

                let deleteFriend = friendsList.pop();
                console.log("Deleted: " + deleteFriend);
                console.log(friendsList);



/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

                let deleteUsingSpliceMethod = friendsList.splice(1);
                console.log("Deleted: " + deleteUsingSpliceMethod);
                console.log(friendsList)






